# Prerequisites
- Bash (tested with version 4.4.20)
- Docker (tested with version 19.03.8)
- certutil (optional, tested with version 2:3.35)
  - `sudo apt-get install libnss3-tools`
  - used to update Network Security Services (NSS) database


# Getting Started
First, generate your CA and dump its certificate. Note that its certificate and
key will be stored in the `devcerts_root` volume.
```bash
./bin/devcerts-ca-init
./bin/devcerts-ca-get-cert > ca.crt
```

Trust your CA by following the [trust tutorial](trust.md).

Generate example project `foop` that will use the hostname `foop.localhost`:
```
./bin/devcerts-create foop.localhost
```

Build and run our example "app". It is a simple Nginx container with `foop`'s
certificate:
```
./bin/devcerts-get foop.localhost example/nginx/certs/
ls -l example/nginx/certs/foop.localhost/
./example/nginx/main.sh
```

Check it out!

- https://foop.localhost
- https://www.foop.localhost
- https://api.foop.localhost

For our terminal junkies:
```
curl https://foop.localhost
curl https://www.foop.localhost
curl https://api.foop.localhost
```


# Example: Traefik
```
./bin/devcerts-get foop.localhost example/traefik/certs/
cd example/traefik/
docker-compose up -d
cd -

curl https://hello.foop.localhost
curl https://traefik.foop.localhost
```
