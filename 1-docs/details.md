# Motivation
Do you develop things that talk over HTTP?

Do you use HTTP**S** in production? (You probably should.)

Maybe you tripped over [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) errors, because your development server looked more like this

http://localhost:8080/

... than this

https://www.my-project.localhost

... then read on!

## Three Steps
All it takes to get nice URLS with SSL on your development machine are these steps:

1. Generate your very own Certificate Authority (CA).
2. Trust your own CA.
3. Profit!

## Overview
There is a single certificate authority (your dev CA) that signs certificates
for different projects.
```mermaid
graph LR
CA -- signs --> foop[*.foop.localhost,foop.localhost]
foop -- used by --> fapp1[foop.localhost]
foop -- used by --> fapp2[www.foop.localhost]
foop -- used by --> fapp3[api.foop.localhost]
CA -- signs --> barp[*.barp.localhost,barp.localhost]
barp -- used by --> bapp1[barp.localhost]
barp -- used by --> bapp2[www.barp.localhost]
CA -- signs --> ...
```

Our example project names are `foop` and `barp`.

By convention host names map to directory names, e.g.
```
foop.localhost --> certs/foop.localhost/
barp.localhost --> certs/barp.localhost/
```


# Tools Used in this Project
- https://github.com/cloudflare/cfssl
- [certutil](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/tools/NSS_Tools_certutil)
  to edit `~/.pki/nssdb`
- Nginx
- openssl (to generate nginx' `dhparams.pem`)


# Cert Store Usage
Different tools use different certificate stores. Here's an incomplete list for
your information:

- `/etc/ssl/certs`
  - `curl`
  - `wget`
  - `openssl`
- `~/.pki/nssdb`
  - Firefox
  - Chrome
