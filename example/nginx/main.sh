#!/bin/bash
set -euo pipefail

here="$(readlink -m $(dirname $0))"
cd $here

[[ -f dhparam.pem ]] || openssl dhparam -out dhparam.pem 1024

set -x
docker build -t foop .
docker rm -f foop || true
docker run -d --name foop -p 80:80 -p 443:443 foop
