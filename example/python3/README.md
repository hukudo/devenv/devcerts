# Initialization
From the root of this project:
```
mkdir -p example/python3/certs/
./bin/devcerts-create python3.localhost
./bin/devcerts-get python3.localhost example/python3/certs/
```


# Running
```
./example/python3/server.py python3.localhost 0.0.0.0:8000
```
