#!/bin/bash
set -euo pipefail

generate_ca() {
  cat <<'EOF' | cfssl genkey -initca -
{
  "CN": "0-main devcerts Root CA",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "O": "0-main.de"
    }
  ]
}
EOF
}

[[ ! -f ca.json ]] && generate_ca > ca.json

mkdir -p certs/
jq -r .cert < ca.json > certs/ca.crt
jq -r .key < ca.json > ca.key

exec $@
